import cv2
import time
import json
import base64
import pickle
import socketio
import mxnet as mx
import numpy as np
from collections import namedtuple

def handleBoundaries(val, maxval):
    return 0 if val < 0 else int(maxval) if val > maxval else val

sio = socketio.Client()

#sio.connect('http://10.0.0.215:3000')
synset_path = 'C:/Users/HP/Desktop/classes.txt'
with open(synset_path, 'r') as f:
    lines = f.readlines()
classes = [l[:-1] for l in lines]

# **model shd be at deployment mode
network_prefix = 'C:/Users/HP/Desktop/detector3'
label_names = ['label']

# Load the network parameters from default epoch 0
sym, arg_params, aux_params = mx.model.load_checkpoint(network_prefix, 0)

# Load the network into an MXNet module and bind the corresponding parameters
mod = mx.mod.Module(symbol=sym, label_names=label_names, context=mx.gpu(0))
mod.bind(for_training=False, data_shapes=[('data', (1,3,512,512))])
arg_params['prob_label'] = mx.nd.array([0])
mod.set_params(arg_params, aux_params)
Batch = namedtuple('Batch', ['data'])

vid = cv2.VideoCapture('C:/Users/HP/Desktop/video.mp4')
frame_skipping = 1
current_time = time.time()

while True:
    
    ret, frame = vid.read()
    
    if not ret:
        break
    
    height, width, _ = frame.shape
    if time.time() - current_time > frame_skipping:
        current_time = time.time() 
        img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        img = cv2.resize(img, (512,512))
        img = np.swapaxes(img, 0, 2)
        img = np.swapaxes(img, 1, 2)
        img = img[np.newaxis, :]
        print('Results:')

        mod.forward(Batch([mx.nd.array(img)]))
        prob = mod.get_outputs()[0].asnumpy()
        prob = np.squeeze(prob)
        print(prob)
        predictions = list()
        for det in prob:
            (klass, score, x1, y1, x2, y2) = det
            if klass == -1:
                continue
            if score < 0.5:
                break
            class_name = classes[int(klass)]
            
            xmin = handleBoundaries(int(x1*width)-30, width) 
            ymin = handleBoundaries(int(y1*height)-30, height)
            xmax = handleBoundaries(int(x2*width)+30, width) 
            ymax = handleBoundaries(int(y2*height)+30, height)
            
            detected_frame = frame[ymin:ymax, xmin:xmax]
            retval, buffer = cv2.imencode('.jpg', detected_frame)
            jpg_as_text = base64.b64encode(buffer)
            
            #sio.emit('message', data=(class_name, str(int(round(score*100))), jpg_as_text.decode()))  
            #sio.emit('data', data={'weapon': class_name, 'percentage': str(int(round(score*100))),
            #                       'img':jpg_as_text.decode()})
            #predictions.append([class_name, score])
        print(predictions)

    cv2.imshow('frame',frame)
    if cv2.waitKey(1) == 27:
        break 
    
        
sio.disconnect()
vid.release()
