import cv2
import time
import json
import base64
import pickle
import socketio
import datetime
import mxnet as mx
import numpy as np
import urllib.request
from collections import namedtuple

tracker = cv2.TrackerKCF_create()

def handleBoundaries(val, maxval):
    return 0 if val < 0 else int(maxval) if val > maxval else val

# standard Python
sio = socketio.Client()

@sio.on('response')
def response(data):
    print('Received message: ' + data)
    current_time_sleep = time.time()
    printOnceResume = True
    while (time.time() - current_time_sleep < 10):
        if printOnceResume:
            print('Resuming machine in 10 secs...')
            printOnceResume = False
    global resume
    resume = True

#sio.connect('http://172.20.10.13:7000')
synset_path = 'C:/Users/HP/Desktop/classes.txt'
with open(synset_path, 'r') as f:
    lines = f.readlines()
classes = [l[:-1] for l in lines]

## Variables definition
resume = True
sleep_5_secs = False
frame_skipping = 0.05
printOnce = True
current_time = time.time()
desire_output = ['knife', 'guns', 'scissors', 'other_weapon', 'metal_pipes', 'catridges', 'rifles', 'other']

## model shd be at deployment mode
network_prefix = 'C:/Users/HP/Desktop/deploy_model_algo_1'
#network_prefix = 'C:/Users/HP/Desktop/detector3'
label_names = ['label']

# Load the network parameters from default epoch 0
sym, arg_params, aux_params = mx.model.load_checkpoint(network_prefix, 0)

# Load the network into an MXNet module and bind the corresponding parameters
mod = mx.mod.Module(symbol=sym, label_names=label_names, context=mx.gpu(0))
mod.bind(for_training=False, data_shapes=[('data', (1,3,512,512))])
mod.set_params(arg_params, aux_params)
Batch = namedtuple('Batch', ['data'])

prob_list = []
cap = cv2.VideoCapture('http://172.20.10.13:8080/?action=stream&ignored.mjpg')

while True:

    ## Waiting for signal to resume detection
    #if not resume:
    #    if printOnce:
    #        print('Waiting for signal...')
    #        printOnce = False
    #    continue

    if (time.time() - current_time > frame_skipping):
        #current_time = time.time() 
        #resp = urllib.request.urlopen('http://10.0.0.231:8080/?action=snapshot&ignored.mjpg')
        #image = np.asarray(bytearray(resp.read()), dtype="uint8")
        ret, frame = cap.read()
        #frame = cv2.imdecode(image, cv2.IMREAD_COLOR)
        height, width, _ = frame.shape

        (success, box) = tracker.update(frame)
        print(success)
        if success:
            xm = int(box[0])
            ym = int(box[1])
            w = int(box[2])
            h = int(box[3])
            p1 = (xm, ym)
            p2 = (handleBoundaries(xm+w, width), handleBoundaries(ym+h, height))
            cv2.rectangle(frame, p1, p2, (0,0,0), 2, 1)
        
        #if success and p2[0] >= width:
            #continue
        frame_cp = frame[:, p2[0]:] if success else frame
        cv2.imshow('frametest', frame_cp)
        
        img = cv2.cvtColor(frame_cp, cv2.COLOR_BGR2RGB)
        img = cv2.resize(img, (512,512))
        img = np.swapaxes(img, 0, 2)
        img = np.swapaxes(img, 1, 2)
        img = img[np.newaxis, :]
        print('Results:')
        print('Predicting...')
        mod.forward(Batch([mx.nd.array(img)]))
        prob = mod.get_outputs()[0].asnumpy()
        prob = np.squeeze(prob)
        print(prob)
        
        predictions = list()

        weapon_list = []
        percentage_list = []
        image_list = []
               
        for det in prob:
            (klass, score, x1, y1, x2, y2) = det
            if klass == -1:
                continue
            if score < 0.20:
                break
            class_name = classes[int(klass)]
            if class_name not in desire_output:
                continue
            
            xmin = handleBoundaries(int(x1*width)-30, width) 
            ymin = handleBoundaries(int(y1*height)-30, height)
            xmax = handleBoundaries(int(x2*width)+30, width) 
            ymax = handleBoundaries(int(y2*height)+30, height)
            prob_list.append(tuple([class_name, xmax, xmin, ymin, xmax-xmin, ymax-ymin]))
            
            detected_frame = frame[ymin:ymax, xmin:xmax]
            retval, buffer = cv2.imencode('.jpg', detected_frame)
            jpg_as_text = base64.b64encode(buffer)
            predictions.append([class_name, score])
            weapon_list.append(class_name)
            percentage_list.append(str(int(round(score*100))))
            image_list.append(jpg_as_text.decode())
            
            try:
                del tracker
            except:
                pass
                
        if len(prob_list) > 0:
            bbox = max(prob_list, key=lambda x:x[1])[2:]
            tracker = cv2.TrackerKCF_create()
            tracker.init(frame, bbox)
            print(bbox)
            prob_list = []

        print('Results - {} '.format(datetime.datetime.now().strftime('%H:%M:%S')))
        print(predictions)
        
        if len(weapon_list) == 0:
            continue
        elif len(weapon_list) == 1:
            weapon_str = weapon_list[0]
            percentage_str = percentage_list[0]
            image_str = image_list[0]
        else:
            weapon_str = '._____.'.join(weapon_list)
            percentage_str = '._____.'.join(percentage_list)
            image_str = '._____.'.join(image_list)
        
        #sio.emit('data', {'weapon': weapon_str, 'percentage': percentage_str, 'img': image_str})
        print('Emitted')
        resume = False
        printOnce = True
        
    cv2.imshow('frame', frame)
    if cv2.waitKey(1) == 27:
        break

sio.disconnect()
cv2.destroyAllWindows()