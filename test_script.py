import cv2
import time
import json
import math
import imutils
import base64
import pickle
import socketio
import mxnet as mx
import numpy as np
from collections import namedtuple

from imutils.video import VideoStream
from imutils.video import FPS

tracker = cv2.TrackerKCF_create()

def handleBoundaries(val, maxval):
    return 0 if val < 0 else int(maxval) if val > maxval else val

sio = socketio.Client()

#sio.connect('http://10.0.0.215:3000')
synset_path = 'C:/Users/HP/Desktop/classes.txt'
with open(synset_path, 'r') as f:
    lines = f.readlines()
classes = [l[:-1] for l in lines]

# **model shd be at deployment mode
network_prefix = 'C:/Users/HP/Desktop/deploy_model_algo_1'
label_names = ['label']

# Load the network parameters from default epoch 0
sym, arg_params, aux_params = mx.model.load_checkpoint(network_prefix, 0)

# Load the network into an MXNet module and bind the corresponding parameters
mod = mx.mod.Module(symbol=sym, label_names=label_names, context=mx.gpu(0))
mod.bind(for_training=False, data_shapes=[('data', (1,3,512,512))])
arg_params['prob_label'] = mx.nd.array([0])
mod.set_params(arg_params, aux_params)
Batch = namedtuple('Batch', ['data'])

vid = cv2.VideoCapture('http://192.168.1.101:8080/?action=stream&ignored.mjpg')
current_time = time.time()
bbox = None

while True:
    
    ret, frame = vid.read()
    
    if not ret:
        break
    frame = imutils.resize(frame, width=512, height=512)[:, 55:]
    height, width, _ = frame.shape
    (success, box) = tracker.update(frame)
    frame_cp = frame
    if success:
        xm = int(box[0])
        ym = int(box[1])
        w = int(box[2])
        h = int(box[3])
        p1 = (xm, ym)
        p2 = (handleBoundaries(xm+w, width), handleBoundaries(ym+h, height))
        cv2.rectangle(frame, p1, p2, (255,0,0), 2, 1)
        frame_cp = frame[:, :p1[0]]
        if p1[0] <= 0:
            frame_cp = frame[:, :p1[0]+20]
    
    cv2.imshow('test frame', frame_cp)
    #cv2.waitKey(1000)
    
    if time.time() - current_time > 1:
        print('Success: ' + str(success))
        current_time = time.time()
        img = cv2.cvtColor(frame_cp, cv2.COLOR_BGR2RGB)
        img = cv2.resize(img, (512,512))
        img = np.swapaxes(img, 0, 2)
        img = np.swapaxes(img, 1, 2)
        img = img[np.newaxis, :]
        print('Results:')

        mod.forward(Batch([mx.nd.array(img)]))
        prob = mod.get_outputs()[0].asnumpy()
        prob = np.squeeze(prob)
        print(prob)
        predictions = list()
        prob_list = []

        for det in prob:

            (klass, score, x1, y1, x2, y2) = det
            if klass == -1:
                continue

            if score < 0.3:
                break
            class_name = classes[int(klass)]

            xmin = handleBoundaries(int(x1*width)-30, width) 
            ymin = handleBoundaries(int(y1*height)-30, height)
            xmax = handleBoundaries(int(x2*width)+30, width) 
            ymax = handleBoundaries(int(y2*height)+30, height)
            prob_list.append(tuple([class_name, xmin, xmin, ymin, xmax-xmin, ymax-ymin]))

            detected_frame = frame[ymin:ymax, xmin:xmax]
            #cv2.imshow('detected', detected_frame)
            retval, buffer = cv2.imencode('.jpg', detected_frame)
            jpg_as_text = base64.b64encode(buffer)

            #sio.emit('message', data=(class_name, str(int(round(score*100))), jpg_as_text.decode()))  
            #sio.emit('data', data={'weapon': class_name, 'percentage': str(int(round(score*100))),
            #                       'img':jpg_as_text.decode()})
            #predictions.append([class_name, score])

        if len(prob_list) > 0:
            bbox = min(prob_list, key=lambda x:x[1])[2:]
            tracker = cv2.TrackerKCF_create()
            tracker.init(frame, bbox)

        print(predictions)

    cv2.imshow('frame',frame)
    if cv2.waitKey(1) == 27:
        break 
    
sio.disconnect()
vid.release()
cv2.destroyAllWindows()
